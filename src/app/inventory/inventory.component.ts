import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {

  products:any;

  constructor(private data: DataService) { }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.data.getAll().then((x:{[key:string]: any}) => {
      this.products = x['products'];
    });
  }

}
